
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name+ '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function stringFromUTF8Array(data)
{
    const extraByteMap = [ 1, 1, 1, 1, 2, 2, 3, 0 ];
    var count = data.length;
    var str = "";
    for (var index = 0;index < count;)
    {
        var ch = data[index++];
        if (ch & 0x80)
        {
            var extra = extraByteMap[(ch >> 3) & 0x07];
            if (!(ch & 0x40) || !extra || ((index + extra) > count))
            return null;
            
            ch = ch & (0x3F >> extra);
            for (;extra > 0;extra -= 1)
            {
            var chx = data[index++];
            if ((chx & 0xC0) != 0x80)
                return null;
            
            ch = (ch << 6) | (chx & 0x3F);
            }
        }
        str += String.fromCharCode(ch);
    }
    return str;
}

Quagga.init({
        inputStream:{
            name:"Live",
            type:"LiveStream",
        },
        decoder:{
            readers:["code_128_reader"],
            multiple: false,
        },
    }, function(err) {
        if (err) {
            console.log(err);
            return
        }
        console.log("Initialization finished. Ready to start");
        Quagga.start();
});

// var allowDetect = true;

Quagga.onDetected(function(result) {
    alert(document.querySelector("#myonoffswitch").checked);
    // setTimeout(function(){
    //     if (!allowDetect){
    //         alert("asdf");
    //         return;
    //     }
    //     else{
    //         allowDetect=false;
    //     }
    // }, 100);


    // window.location.replace("./confirm.html");
    // if (result.codeResult) {
    //     var resultString = stringFromUTF8Array(base64js.toByteArray(result.codeResult.code));
    //     var name = resultString.substr(2);
    //     var level = resultString.charAt(0);
    //     var xmlHttp = new XMLHttpRequest();
    //     xmlHttp.open( "GET", "https://maker.ifttt.com/trigger/broadcasting_check/with/key/UwMYktAWEieocuUeNqYeP?value1=".concat(getParameterByName("item-id"),"&value2=",getParameterByName("uuid"),"&value3=",name), false );
    //     // xmlHttp.open( "GET", "https://maker.ifttt.com/trigger/broadcasting_check/with/key/czCBWMOiD3OutcFJxYmqI2?value1=".concat(getParameterByName("item-id"),"&value2=",getParameterByName("uuid"),"&value3=",name), false );
    //     xmlHttp.send( null );
    //     setTimeout(function() {
    //         throw new Error("Please don't execute this function again. Ever.");
    //         window.location.replace("./confirm.html");
    //     }, 1000);
    // }
});

Quagga.onProcessed(function(callback) {
    var drawingCtx = Quagga.canvas.ctx.overlay;
    var drawingCanvas = Quagga.canvas.dom.overlay;
    if (callback){
        console.log(callback);
        if (callback.box) {
            drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute("width")), parseInt(drawingCanvas.getAttribute("height")));
            Quagga.ImageDebug.drawPath(callback.box, {x: 0, y: 1}, drawingCtx, {color: "#00F", lineWidth: 2});
        }
    }
});
